/**
 * @jest-environment jsdom
 */

import MultimediaObject from "./index";

const driver = class Driver {
  constructor(n) {
    this.namespace = n || {};
    this.id = "test";
  }
  init() {}
  update() {}
  exportToJSON() {}
};

class Driver2 {
  constructor(n) {
    this.namespace = n || {};
    this.id = "test2";
  }
  init() {}
  update() {}
  exportToJSON() {}
}

describe("MultimediaObject.$hooks", () => {
  test("should execute $hook with correct context and arguments", () => {
    const hookMock = jest.fn();
    const params = {
      name: "test",
      titi: "test",
      randomKey: {
        test: "inside",
      },
      data: {
        drivers: {
          test: {
            $hooks: [
              {
                type: 'init',
                fn: hookMock
              }
            ]
          }
        }
      },
      childs: [
        {
          name: "test",
          titi: "test",
          randomKey: {
            test: "inside",
          },
          data: {
            drivers: {
              DOM: {},
            },
          },
        },
      ],
      drivers: [{ id: "test", driver }],
      events: {
        test: () => "test",
      },
      functions: {
        test: () => "test",
      },
    };

    const mo = new MultimediaObject(params);

    expect(hookMock).toHaveBeenCalledWith(mo);
  })
})

describe("MultimediaObject.constructor", () => {
  test("should instanciate a MultimediaObject", () => {
    const mo = new MultimediaObject();

    expect(mo).toBeInstanceOf(MultimediaObject);
    expect(mo.name).toEqual(`multimediaobject-${mo.uuid}`);
  });

  test("should instanciate a MultimediaObject and load drivers", () => {
    const mo = new MultimediaObject();

    expect(mo.drivers).toHaveLength(0);
    expect(mo.data.drivers.DOM).not.toBeDefined();
  });
});

describe("MultimediaObject.loadDrivers", () => {
  test("should add drivers to array if not already present", () => {
    const mo = new MultimediaObject();

    const originalError = console.error;
    console.error = jest.fn();

    mo.addDriver("test", driver);

    expect(mo.drivers).toHaveLength(1);
    expect(mo.data.drivers.test).toBeInstanceOf(Object);

    mo.addDriver("test", driver);

    expect(mo.drivers).toHaveLength(1);
    expect(console.error).toHaveBeenCalled();

    console.error = originalError;
  });
  test("should throw if not array", () => {
    const mo = new MultimediaObject();
    expect(() => mo.loadDrivers("test")).toThrowError(/should be of type/);
  });
});

describe("MultimediaObject.removeDriver", () => {
  test("should remove a driver by id if present", () => {
    const mo = new MultimediaObject();

    const originalError = console.error;
    console.error = jest.fn();

    mo.loadDrivers([
      { id: "test", driver },
      { id: "test2", driver: Driver2 },
    ]);

    expect(mo.drivers).toHaveLength(2);

    mo.removeDriver("test2");

    expect(mo.drivers).toHaveLength(1);
    mo.removeDriver("test2");
    expect(mo.drivers).toHaveLength(1);
    expect(console.error).toHaveBeenCalled();
    console.error = originalError;
  });
});

describe("MultimediaObject.setAbsoluteUrl", () => {
  afterEach(() => {
    delete window.MultimediaObjectEditor;
    delete window.moConfig;
  });

  test("should not apply anything if window[configuration.namespace] is undefined", () => {
    window.MultimediaObjectEditor = true;
    window.moConfig = {};

    const data = {
      template: false,
      load: true,
    };

    const mo = new MultimediaObject(data);

    expect(mo.data.absoluteAssetURL).toBeUndefined();
  });

  test("should set absoluteAssetURL if window[namespace].absoluteAssetURL is set and no template", () => {
    window.MultimediaObjectEditor = true;
    window.moConfig = {};
    window.moConfig.absoluteAssetURL = "test";

    const data = {
      template: false,
      load: true,
    };

    const mo = new MultimediaObject(data);

    expect(mo.data.absoluteAssetURL).toEqual("test");
  });

  test("should apply templateURL if it is set", () => {
    window.MultimediaObjectEditor = true;
    window.moConfig = {};
    window.moConfig.absoluteAssetURL = "test";

    const data = {
      load: true,
      data: {
        templateURL: "test",
        template: true,
      },
    };

    const mo = new MultimediaObject(data);

    expect(mo.data.absoluteAssetURL).toEqual("test");
  });

  test("should apply window[namespace].absoluteAssetURL if it is set on non MultimediaObjectEditor context", () => {
    window.moConfig = {};
    window.moConfig.absoluteAssetURL = "test";

    const data = {
      load: true,
      data: {
        templateURL: "failed test !",
        template: true,
      },
    };

    const mo = new MultimediaObject(data);

    expect(mo.data.absoluteAssetURL).toEqual("test");
  });

  test("should apply './' if window[namespace].absoluteAssetURL is not set on non MultimediaObjectEditor context", () => {
    window.moConfig = {
      absoluteAssetURL: "undefined",
    };
    const data = {
      load: true,
      data: {
        templateURL: "test",
      },
    };

    const mo = new MultimediaObject(data);
    expect(mo.data.absoluteAssetURL).toEqual("./");
  });
});

describe("MultimediaObject.loadFromJSON", () => {
  test("should copy keys [childs, drivers, events, functions]", () => {
    const params = {
      name: "test",
      titi: "test",
      randomKey: {
        test: "inside",
      },
      childs: [
        {
          name: "test",
          titi: "test",
          randomKey: {
            test: "inside",
          },
          data: {
            drivers: {
              DOM: {},
            },
          },
        },
      ],
      drivers: [{ id: "test", driver }],
      events: {
        test: () => "test",
      },
      functions: {
        test: () => "test",
      },
    };

    const mo = new MultimediaObject(params);

    expect(mo.name).toEqual("test");
    expect(mo.titi).not.toBeDefined();
    expect(mo.randomKey).not.toBeTruthy();
    expect(mo.randomKey).not.toBeDefined();
    expect(mo.childs).toHaveLength(1);
    expect(mo.childs[0].titi).not.toBeDefined();
    expect(mo.events).toBeDefined();
    expect(mo.test()).toEqual("test");
    expect(mo.functions).toBeDefined();
    expect(mo.test).toBeDefined();
    expect(mo.events.test()).toEqual("test");
  });

  test("should unserialize events and add them to this.events", () => {
    const params = {
      exportedEvents: {
        click: {
          args: [""],
          body: "return 'test'",
        },
      },
    };

    const mo = new MultimediaObject(params);

    expect(mo.events).toHaveProperty("click");
    expect(mo.events.click).toBeInstanceOf(Function);
    expect(mo.events.click()).toEqual("test");
  });

  test("should unserialize functions and add them to this.functions", () => {
    const params = {
      exportedFunctions: {
        logMe: {
          args: ["test"],
          body: "return test",
        },
      },
    };

    const mo = new MultimediaObject(params);

    expect(mo.functions).toHaveProperty("logMe");
    expect(mo.functions.logMe).toBeInstanceOf(Function);
    expect(mo.functions.logMe("test")).toEqual("test");
  });

  test("should not throw error if function is badly formed", () => {
    const originalError = console.error;
    console.error = jest.fn();

    const params = {
      exportedFunctions: {
        logMe: {
          args: ["test"],
          body: "return test var function",
        },
      },
    };

    expect(() => new MultimediaObject(params)).not.toThrow();

    const mo = new MultimediaObject(params);

    expect(mo.functions.logMe).toBeInstanceOf(Function);
    expect(console.error).toHaveBeenCalled();
    console.error = originalError;
  });

  test('should unserialize childs and add them as new MultimediaObjects and add ["DOMParent"] keys', () => {
    const params = {
      childs: [
        {
          name: "test",
        },
        {
          name: "test2",
        },
      ],
      data: {
        drivers: {
          DOM: {
            element: "test",
          },
        },
      },
    };

    const mo = new MultimediaObject(params);

    expect(mo.childs).toHaveLength(2);
    expect(mo.childs[0]).toBeTruthy();
    expect(mo.childs[1]).toBeTruthy();
    expect(mo.childs[0]).toBeInstanceOf(MultimediaObject);
    expect(mo.childs[1]).toBeInstanceOf(MultimediaObject);
    expect(mo.childs[0].name).toEqual("test");
    expect(mo.childs[1].name).toEqual("test2");
  });

  test("should convert booleans in data", () => {
    const data = {
      test: "false",
      test1: "true",
    };

    const mo = new MultimediaObject({
      data,
    });

    expect(mo.data.test).toEqual(false);
    expect(mo.data.test1).toEqual(true);
  });
});

describe("MultimediaObject.update", () => {
  test("should only the updated driver update", () => {
    let test1 = false;
    let test2 = false;
    class D1 {
      constructor(n) {
        this.namespace = n || {};
        this.id = "DOM";
      }
      init() {
        return jest.mock();
      }
      update() {
        test1 = true;
      }
    }
    class D2 {
      constructor(n) {
        this.namespace = n || {};
        this.id = "CSS";
      }
      init() {
        return jest.mock();
      }
      update() {
        test2 = true;
      }
    }
    const drivers = [
      { id: "DOM", driver: D1 },
      { id: "CSS", driver: D2 },
    ];
    const mo = new MultimediaObject({
      drivers,
    });

    mo.updateDriver("DOM", "update", {});
    expect(test1).toBe(true);
    expect(test2).toBe(false);
  });
});

describe("MultimediaObject.applyFunctions", () => {
  test("should apply the functions and bind them to the MO", () => {
    const mo = new MultimediaObject({
      functions: {
        logMe() {
          return "test";
        },
      },
    });

    expect(mo.logMe).toBeDefined();
    expect(mo.logMe()).toEqual("test");
  });

  test("should use console.error to say if a function fails", () => {
    const oConsole = console;
    console.error = jest.fn();

    const params = {
      functions: {
        applyStyle: "",
      },
    };
    const mo = new MultimediaObject(params);

    expect(mo.applyStyle).not.toBeDefined();

    expect(console.error).toHaveBeenCalled();

    console.error = oConsole.error;
  });
});

describe("MultimediaObject.exportToJSON", () => {
  it("should unserialize basic data without default name", () => {
    class driver1 {
      constructor(n) {
        this.namespace = n || {};
        this.id = "driver1";
      }
      init() {
        return jest.mock();
      }
      update() {
        return jest.mock();
      }
      exportToJSON() {}
    }

    const mo = new MultimediaObject({
      functions: {
        logMe() {
          return "test";
        },
      },
      events: {
        click() {
          return "test";
        },
      },
      drivers: [{ id: "driver1", driver: driver1 }],
      childs: [
        {
          functions: {
            logMe() {
              return "test";
            },
          },
          events: {
            click() {
              return "test";
            },
          },
          drivers: [{ id: "driver1", driver: driver1 }],
        }
      ]
    });

    expect(mo).toBeDefined();

    const exportedMO = mo.exportToJSON();

    // console.log(exportedMO);

    expect(exportedMO.name).not.toBe(`multimediaobject-${mo.uuid}`);
    expect(exportedMO.childs).toBeDefined();
  });

  it("should unserialize basic data with name", () => {
    class driver1 {
      constructor(n) {
        this.namespace = n || {};
        this.id = "driver1";
      }
      init() {
        return jest.mock();
      }
      update() {
        return jest.mock();
      }
      exportToJSON() {}
    }

    const mo = new MultimediaObject({
      name: "test_name",
      functions: {
        logMe() {
          return "test";
        },
      },
      events: {
        click() {
          return "test";
        },
      },
      drivers: [{ id: "driver1", driver: driver1 }],
    });

    expect(mo).toBeDefined();

    const exportedMO = mo.exportToJSON();

    expect(exportedMO.name).toBe(mo.name);
  });
});

export default ({
  "id": String,
  "init": Function,
  "update": Function,
  "exportToJSON": Function,
});
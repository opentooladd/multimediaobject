import configuration from "./config/config";
import check from "./libs/check/dist/index";
import parseBooleans from "./libs/microutils/dist/lang/parseBooleans";
import uuid from "./libs/microutils/dist/lang/uuid";
import unserialize from "./libs/microutils/dist/functions/unserialize";
import serialize from "./libs/microutils/dist/functions/serialize";

import constructorSchema from "./schemas/constructor";
const default_name_regex = new RegExp(/multimediaobject-.+/, "i");

export default class MultimediaObject {
  constructor(conf = {}) {
    check.validate(conf, constructorSchema);
    this.uuid = conf.uuid || uuid();
    this.name = conf.name || `multimediaobject-${this.uuid}`;

    this.data = conf.data || {};
    this.data.drivers = this.data.drivers || {};

    this.events = conf.events || {};
    this.functions = conf.functions || {};
    this.drivers = [];
    this.breakpoints = [];
    this.dependencies = [];
    this.childs = [];

    this.loadFromJSON(conf);
    if (conf.drivers) this.loadDrivers(conf.drivers);
    this._init();
  }

  /**
   * Initialize the MultimediaObject
   * load and initialise all drivers
   * on there namespace
   *
   * @memberOf MultimediaObject
   */
  _init() {
    this.applyFunctions();
    this.drivers
      .filter((d) => d.namespace.$hooks)
      .forEach((d) =>
        d.namespace.$hooks
          .filter((h) => h.type === "init")
          .forEach((h) => h.fn.apply(d, [this]))
      );
    this.childs.forEach((c) => c._init());
    this.applyEvents();
  }

  triggerHooks(type) {
    this.drivers
      .filter((d) => d.namespace.$hooks)
      .forEach((d) =>
        d.namespace.$hooks
          .filter((h) => h.type === type)
          .forEach((h) => h.fn.apply(d, [this]))
      );
    this.childs.forEach((c) => c.triggerHooks(type));
  }

  /**
   * launch the update cycle
   * trigger driver update
   *
   * @memberOf MultimediaObject
   */
  updateDriver(driverId, methodName, ...args) {
    const driver = this.drivers.find((d) => d.id === driverId);
    if (driver) {
      if (methodName) driver[methodName].call(driver, ...args);
      if (driver.update) driver.update(this);
    } else {
      console.error(`${driverId} doesn't exist`);
    }
    return this;
  }

  /**
   * get a MultimediaObjec driver
   *
   * @memberOf MultimediaObject
   */
  getDriver(driverId) {
    return this.drivers.find((d) => d.id === driverId);
  }

  /**
   * load a Driver into the MultimediaObject
   * this driver gets is own personnal space into the data.drivers namespace
   *
   * @memberOf MultimediaObject
   * @param {[Driver]} driver - a Driver configuration
   * @return {MultimediaObject} this
   */
  loadDrivers(drivers) {
    check.check(drivers, Array);
    drivers.forEach(({ id, driver }) => this.addDriver(id, driver));
  }

  /**
   * applyFunction applies this.functions to the Object
   * binding each context to the Object
   *
   * @memberOf MultimediaObject
   */
  applyFunctions() {
    for (const fName in this.functions) {
      try {
        check.check(this.functions[fName], Function);
        this.functions[fName] = this.functions[fName].bind(this);
        this[fName] = this.functions[fName];
      } catch (e) {
        console.error(e);
      }
    }
  }

  /**
   * applyEvents applies this.events to the Object
   * binding each context to the Object
   *
   * @memberOf MultimediaObject
   */
  applyEvents() {
    for (const eName in this.events) {
      try {
        check.check(this.events[eName], Function);
        this.events[eName] = this.events[eName].bind(this);
      } catch (e) {
        console.error(e);
      }
    }
  }

  /**
   * add a Driver to the MultimediaObject if it does not exist
   * create a personnal space fot he driver to access
   *
   * @param {Driver} driver - a driver configuration
   *
   * @memberOf MultimediaObject
   */
  addDriver(driverId, driver) {
    check.check(driver, Function);
    const existingDriver = this.drivers.find((d) => d.id === driverId);
    if (!existingDriver) {
      const namespace = this.data.drivers[driverId] || {};
      namespace.moId = this.uuid;
      namespace.MOParent = this.data.MOParent;
      this.data.drivers[driverId] = namespace;
      const d = new driver(namespace);
      this.drivers.push(d);
      this.childs.forEach((c) => c.addDriver(driverId, driver));
    } else {
      console.error(`Driver ${driverId} is already loaded`);
    }
  }

  /**
   * remove a Driver driver from a MultimediaObject
   * deletes his data space
   *
   * @param {Driver} driver
   *
   * @memberOf MultimediaObject
   */
  removeDriver(id) {
    const index = this.drivers.findIndex((d) => d.id === id);
    if (index >= 0) {
      this.drivers.splice(index, 1);
      delete this.data.drivers[id];
    } else {
      console.error(`Driver ${id} does not exist`);
    }
  }

  /**
   * Set the absoluteAssetURL of the object
   * @param {object} json - the json representation of a MultimediaObject
   */

  setAbsoluteAssetURL() {
    if (window[configuration.globalNamespace]) {
      if (!this.data.template && window[configuration.namespace]) {
        if (
          typeof window[configuration.namespace].absoluteAssetURL !==
            "undefined" &&
          window[configuration.namespace].absoluteAssetURL !== "undefined" &&
          window[configuration.namespace].absoluteAssetURL !== ""
        ) {
          this.data.absoluteAssetURL =
            window[configuration.namespace].absoluteAssetURL;
        }
      } else {
        this.data.absoluteAssetURL = this.data.templateURL;
      }
    } else if (
      !window[configuration.globalNamespace] &&
      window[configuration.namespace]
    ) {
      if (
        typeof window[configuration.namespace].absoluteAssetURL !==
          "undefined" &&
        window[configuration.namespace].absoluteAssetURL !== "undefined" &&
        window[configuration.namespace].absoluteAssetURL !== ""
      ) {
        this.data.absoluteAssetURL =
          window[configuration.namespace].absoluteAssetURL;
      } else {
        this.data.absoluteAssetURL = this.data.absoluteAssetURL || "./";
      }
    }
  }

  /**
   * Return a JSON object of the data
   * This is meant to be implemented by the user to save a specific set of data
   * that he knows how to serialize
   */

  exportDataToJSON() {
    return {};
  }

  /**
   * Export the object as a reusable JSON
   * @return {object} the JSON representation of the object
   */

  exportToJSON() {
    const ob = {};
    ob.exportedEvents = {};
    ob.exportedFunctions = {};
    ob.exportedDrivers = {};
    ob.childs = [];

    for (const evt in this.events) {
      ob.exportedEvents[evt] = serialize(this.events[evt]);
    }

    for (const func in this.functions) {
      ob.exportedFunctions[func] = serialize(this.functions[func]);
    }

    this.childs.forEach((child) => {
      const c = child.exportToJSON();
      ob.childs.push(c);
    });

    ob.breakpoints = this.breakpoints;
    ob.dependencies = this.dependencies;
    ob.data = {
      drivers: {},
    };
    this.drivers.forEach((d) => (ob.data.drivers[d.id] = d.exportToJSON()));
    ob.data = {
      ...ob.data,
      ...this.exportDataToJSON(),
    };
    ob.type = this.type;
    if (!default_name_regex.test(this.name)) ob.name = this.name;

    return JSON.parse(JSON.stringify(ob));
  }

  /**
   * load a JSON representation of a MultimediaObject
   * @param {object} json - a json representation of a MultimediaObject
   */

  loadFromJSON(json) {
    for (const evt in json.exportedEvents) {
      this.events[evt] = unserialize(json.exportedEvents[evt]);
    }

    for (const func in json.exportedFunctions) {
      this.functions[func] = unserialize(json.exportedFunctions[func]);
    }

    if (json.childs) {
      json.childs.forEach((child) => {
        child.data = child.data || {};
        child.data.MOParent = this;
        this.childs.push(new MultimediaObject(child));
      });
    }
    // convert Booleans to Booleans
    Object.keys(this.data).forEach((key) => {
      if (this.data[key] === "false" || this.data[key] === "true")
        this.data[key] = parseBooleans(this.data[key]);
    });
    this.setAbsoluteAssetURL();
  }
}

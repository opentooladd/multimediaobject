export default {
  globalNamespace: '{{ globalnamespace }}',
  namespace: '{{ namespace }}',
  container: '{{ container }}',
  // globalNamespace: 'MultimediaObjectEditor',
  // namespace: 'moConfig',
  // container: 'scene',
  defaultAttributes: {
    video: {
      src: '',
      type: 'video/mp4',
      controls: 'true',
      muted: 'true',
      preload: 'true',
      playsinline: 'true',
    },
    img: {
      src: '',
    },
    iframe: {
      src: '',
    },
    form: {
      name: '',
    },
    input: {
      name: '',
    },
    audio: {
      src: '',
    },
  },
};
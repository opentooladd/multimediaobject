import { nodeResolve } from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import terser from '@rollup/plugin-terser';

const baseConfig = {
  input: 'src/index.js',
  output: {
    name: 'MultimediaObject',
    file: 'dist/index.js',
    format: 'es',
  },
  plugins: [
    nodeResolve(),
    commonjs({
      include: 'node_modules/**',
    }),
  ],
};

export default [
  baseConfig,
  {
    ...baseConfig,
    output: {
      ...baseConfig.output,
      file: 'dist/index.umd.js',
      format: 'umd',
    }
  },
  {
    ...baseConfig,
    output: {
      ...baseConfig.output,
      file: 'dist/index.iife.js',
      format: 'iife',
    }
  },
  {
    ...baseConfig,
    output: {
      ...baseConfig.output,
      file: 'dist/index.cjs.js',
      format: 'cjs',
    }
  },
  // min config
  {
    ...baseConfig,
    output: {
      ...baseConfig.output,
      file: 'dist/index.min.js',
    },
    plugins: [
      ...baseConfig.plugins,
      terser()
    ]
  },
  {
    ...baseConfig,
    output: {
      ...baseConfig.output,
      file: 'dist/index.umd.min.js',
      format: 'umd',
    },
    plugins: [
      ...baseConfig.plugins,
      terser()
    ]
  },
  {
    ...baseConfig,
    output: {
      ...baseConfig.output,
      file: 'dist/index.iife.min.js',
      format: 'iife',
    },
    plugins: [
      ...baseConfig.plugins,
      terser()
    ]
  },
  {
    ...baseConfig,
    output: {
      ...baseConfig.output,
      file: 'dist/index.cjs.min.js',
      format: 'cjs',
    },
    plugins: [
      ...baseConfig.plugins,
      terser()
    ]
  }
];
(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
  typeof define === 'function' && define.amd ? define(factory) :
  (global = typeof globalThis !== 'undefined' ? globalThis : global || self, global.MultimediaObject = factory());
})(this, (function () { 'use strict';

  var configuration = {
    globalNamespace: 'MultimediaObjectEditor',
    namespace: 'moConfig',
    container: 'scene',
    defaultAttributes: {
      video: {
        src: '',
        type: 'video/mp4',
        controls: 'true',
        muted: 'true',
        preload: 'true',
        playsinline: 'true',
      },
      img: {
        src: '',
      },
      iframe: {
        src: '',
      },
      form: {
        name: '',
      },
      input: {
        name: '',
      },
      audio: {
        src: '',
      },
    },
  };

  var difference = (source, inputs) => {
    const flatArray = inputs.filter(i => (i !== null && typeof i !== 'undefined')).map(i => i instanceof Object ? JSON.stringify(i) : i);
    let diffs = source
    .filter(i => (i !== null && typeof i !== 'undefined'))
    .filter((i) => {
      const e = i instanceof Object ? JSON.stringify(i) : i;
      const index = flatArray.indexOf(e);
      if (index >= 0) flatArray.splice(index, 1);
      return !(index >= 0);
    });
    return diffs;
  };

  const nRegEx = new RegExp(/[\-\+]{0,1}\d+(\.\d+)?/, 'g');
  const maxRegEx = new RegExp(/^>\d+(\.\d+)?$/);
  const minRegEx = new RegExp(/^<\d+(\.\d+)?$/);
  const rangeRegEx = new RegExp(/^\d+(\.\d+)?<>\d+(\.\d+)?$/);

  var index = ({
    validate(input, schema, strict = false) {
      const keys = Object.keys(schema);
      const notVarKeys = keys.filter(k => !k.includes('?'));
      const inputKeys = Object.keys(input);
      const keyDifference = difference(notVarKeys, inputKeys);

      if (keyDifference.length > 0) return this.logError({
        input,
        source: keys,
        valid: false,
        message: `ValidationError: ${JSON.stringify(input)} missing keys [${keyDifference}]`
      });

      inputKeys.forEach(key => {
        const source = (schema[key] || schema[`?${key}`]);
        if (source) this.check(input[key], source);
        else if (!source && strict) return this.checkArray(inputKeys, keys);
      });

      return null;
    },
    check(input, source) {
      if (typeof source !== 'undefined' && source !== null) {
        if (source instanceof Function) return this.checkType(input, source);
        const numberTest = (maxRegEx.test(source) || minRegEx.test(source) || rangeRegEx.test(source)) || this.getType(source) === 'Number';
        const stringTest = typeof source === 'string' || source instanceof RegExp || this.getType(source) === 'String';
        const arrayTest = source instanceof Array;

        if (numberTest) {
          return this.checkNumber(input, source);
        } else if (stringTest) {
          return this.checkString(input, source);
        } else if (arrayTest) {
          const typeArray = source.filter(o => o instanceof Function);
          if (typeArray.length > 0 && typeArray.length === source.length) {
            let success = false;
            for (let i = 0; i < typeArray.length; i++) {
              try {
                success = true;
                this.checkType(input, typeArray[i]);
                break;
              } catch (e) {
                success = false;
              }
            }
            if (!success) {
              return this.logError({
                input,
                source,
                valid: false,
                message: `${input} should be of type ${typeArray.map(a => this.getType(a))}`
              });
            }
            return null;
          } else {
            return this.checkArray(input, source);
          }
        } else {
          return this.checkObject(input, source);
        }
      }
      return null;
    },
    checkType(input, source) {
      const t = input ? `${(this.getType(source)[0]).toLowerCase()}${this.getType(source).slice(1)}` : true;
      const tInput = input ? `${(input.constructor.name[0]).toLowerCase()}${input.constructor.name.slice(1)}` : false;
      if (t !== tInput) {
        return this.logError({
          input,
          source,
          valid: false,
          message: `${JSON.stringify(input)} should be of type ${this.getType(source)}`
        });
      }
      return null;
    },
    checkString(input, source) {
      if (source instanceof RegExp) {
        return this.logError({
          input,
          source,
          valid: source.test(input),
        });
      } else {
        return this.logError({
          input,
          source,
          valid: source === input,
        });
      }
    },
    checkNumber(input, source) {
      const minTest = minRegEx.test(source);
      const maxTest = maxRegEx.test(source);
      const rangeTest = rangeRegEx.test(source);
      if (rangeTest && !minTest && !maxTest) {
        const matchMin = source.match(nRegEx)[0];
        const min = parseFloat(matchMin);
        const matchMax = source.match(nRegEx)[1];
        const max = parseFloat(matchMax);
        return this.logError({
          input,
          source,
          valid: input < max && input > min,
        });
      } else if (maxTest && !minTest && !rangeTest) {
        const match = source.match(nRegEx)[0];
        const n = parseFloat(match);
        return this.logError({
          input,
          source,
          valid: input > n,
        });
      } else {
        const match = source.match(nRegEx)[0];
        const n = parseFloat(match);
        return this.logError({
          input,
          source,
          valid: input !== '' && typeof input !== 'undefined' && input !== null && input < n,
        });
      }
    },
    checkArray(input, source) {
      const diffs = difference(source, input);
      return this.logError({
        input: JSON.stringify(input),
        source: JSON.stringify(source),
        valid: diffs.length === 0,
      });
    },
    checkObject(input, source) {
      return this.validate(input, source);
    },
    logError(error) {
      const message = `ValidationError: ${error.input} type:${typeof error.input} must respect ${error.source} type:${typeof error.source}`;
      if (!error.valid) {
        throw new TypeError(error.message || message);
      }
      return null;
    },
    getType(source) {
      return source.name || typeof source;
    }
  });

  var check = index;

  var parseBooleans = (string) => {
    if (string === '') {
      return true;
    }
    if (typeof string === 'undefined' || string === 'false' || Boolean(string) === false) {
      return false;
    }
    return Boolean(string);
  };

  var parseBooleans$1 = parseBooleans;

  var uuid = () => {
    let d = new Date().getTime();
    if (window.performance && typeof window.performance.now === 'function') {
      d += performance.now(); // use high-precision timer if available
    }
    const uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
      const r = (d + Math.random() * 16) % 16 | 0;
      d = Math.floor(d / 16);
      return (c === 'x'
              ? r
              : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
  };

  var uuid$1 = uuid;

  const COMMENT_REG_EXP$1 = new RegExp(/\n+|(\/\*\*\/\n)+/, 'g');
  const NEW_LINE_REG_EXP$1 = new RegExp(/^(\n+|\t+|\t\n+)(?!\w)$/, 'gm');
  const BACKTICK_REG_EXP$1 = new RegExp(/`/, 'gm');
  const COMMENTS_AND_BACKTICK = new RegExp(/(\/\*``\*\/)+/, 'g');

  var unserialize = (serialized) => {
    const args = serialized.args.map(el => el.replace(COMMENT_REG_EXP$1, '').replace(COMMENTS_AND_BACKTICK, '').replace(NEW_LINE_REG_EXP$1, '').replace(BACKTICK_REG_EXP$1, ''));
    const { body } = serialized;
    let func = new Function();
    try {
      func = new Function(args, body);
    } catch (e) {
      console.error(e);
    }
    return func;
  };

  var unserialize$1 = unserialize;

  const COMMENT_REG_EXP = new RegExp(/\n+|(\/\*\*\/\n)+/, 'g');
  const NEW_LINE_REG_EXP = new RegExp(/^(\n+|\t+|\t\n+)(?!\w)$/, 'gm');
  const BACKTICK_REG_EXP = new RegExp(/`/, 'gm');

  const serialize = (funct) => {
    if (!(funct instanceof Function)) throw new TypeError('serialize: argument should be a function');
    const txt = funct.toString();
    const args = txt.slice(txt.indexOf('(') + 1, txt.indexOf(')')).split(',');
    const body = txt.slice(txt.indexOf('{') + 1, txt.lastIndexOf('}'));
    return {
      args: args.map(el => el.replace(COMMENT_REG_EXP, '')
        .replace(NEW_LINE_REG_EXP, '')
        .replace(BACKTICK_REG_EXP, '')),
      body,
    };
  };

  var serialize$1 = serialize;

  var constructorSchema = ({
    "?name": new RegExp("^[a-zA-Z0-9\-_]{1,}$"),
    "?uuid": new RegExp(/^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/, 'i'),
    "?data": Object,
  });

  const default_name_regex = new RegExp(/multimediaobject-.+/, "i");

  class MultimediaObject {
    constructor(conf = {}) {
      check.validate(conf, constructorSchema);
      this.uuid = conf.uuid || uuid$1();
      this.name = conf.name || `multimediaobject-${this.uuid}`;

      this.data = conf.data || {};
      this.data.drivers = this.data.drivers || {};

      this.events = conf.events || {};
      this.functions = conf.functions || {};
      this.drivers = [];
      this.breakpoints = [];
      this.dependencies = [];
      this.childs = [];

      this.loadFromJSON(conf);
      if (conf.drivers) this.loadDrivers(conf.drivers);
      this._init();
    }

    /**
     * Initialize the MultimediaObject
     * load and initialise all drivers
     * on there namespace
     *
     * @memberOf MultimediaObject
     */
    _init() {
      this.applyFunctions();
      this.drivers
        .filter((d) => d.namespace.$hooks)
        .forEach((d) =>
          d.namespace.$hooks
            .filter((h) => h.type === "init")
            .forEach((h) => h.fn.apply(d, [this]))
        );
      this.childs.forEach((c) => c._init());
      this.applyEvents();
    }

    triggerHooks(type) {
      this.drivers
        .filter((d) => d.namespace.$hooks)
        .forEach((d) =>
          d.namespace.$hooks
            .filter((h) => h.type === type)
            .forEach((h) => h.fn.apply(d, [this]))
        );
      this.childs.forEach((c) => c.triggerHooks(type));
    }

    /**
     * launch the update cycle
     * trigger driver update
     *
     * @memberOf MultimediaObject
     */
    updateDriver(driverId, methodName, ...args) {
      const driver = this.drivers.find((d) => d.id === driverId);
      if (driver) {
        if (methodName) driver[methodName].call(driver, ...args);
        if (driver.update) driver.update(this);
      } else {
        console.error(`${driverId} doesn't exist`);
      }
      return this;
    }

    /**
     * get a MultimediaObjec driver
     *
     * @memberOf MultimediaObject
     */
    getDriver(driverId) {
      return this.drivers.find((d) => d.id === driverId);
    }

    /**
     * load a Driver into the MultimediaObject
     * this driver gets is own personnal space into the data.drivers namespace
     *
     * @memberOf MultimediaObject
     * @param {[Driver]} driver - a Driver configuration
     * @return {MultimediaObject} this
     */
    loadDrivers(drivers) {
      check.check(drivers, Array);
      drivers.forEach(({ id, driver }) => this.addDriver(id, driver));
    }

    /**
     * applyFunction applies this.functions to the Object
     * binding each context to the Object
     *
     * @memberOf MultimediaObject
     */
    applyFunctions() {
      for (const fName in this.functions) {
        try {
          check.check(this.functions[fName], Function);
          this.functions[fName] = this.functions[fName].bind(this);
          this[fName] = this.functions[fName];
        } catch (e) {
          console.error(e);
        }
      }
    }

    /**
     * applyEvents applies this.events to the Object
     * binding each context to the Object
     *
     * @memberOf MultimediaObject
     */
    applyEvents() {
      for (const eName in this.events) {
        try {
          check.check(this.events[eName], Function);
          this.events[eName] = this.events[eName].bind(this);
        } catch (e) {
          console.error(e);
        }
      }
    }

    /**
     * add a Driver to the MultimediaObject if it does not exist
     * create a personnal space fot he driver to access
     *
     * @param {Driver} driver - a driver configuration
     *
     * @memberOf MultimediaObject
     */
    addDriver(driverId, driver) {
      check.check(driver, Function);
      const existingDriver = this.drivers.find((d) => d.id === driverId);
      if (!existingDriver) {
        const namespace = this.data.drivers[driverId] || {};
        namespace.moId = this.uuid;
        namespace.MOParent = this.data.MOParent;
        this.data.drivers[driverId] = namespace;
        const d = new driver(namespace);
        this.drivers.push(d);
        this.childs.forEach((c) => c.addDriver(driverId, driver));
      } else {
        console.error(`Driver ${driverId} is already loaded`);
      }
    }

    /**
     * remove a Driver driver from a MultimediaObject
     * deletes his data space
     *
     * @param {Driver} driver
     *
     * @memberOf MultimediaObject
     */
    removeDriver(id) {
      const index = this.drivers.findIndex((d) => d.id === id);
      if (index >= 0) {
        this.drivers.splice(index, 1);
        delete this.data.drivers[id];
      } else {
        console.error(`Driver ${id} does not exist`);
      }
    }

    /**
     * Set the absoluteAssetURL of the object
     * @param {object} json - the json representation of a MultimediaObject
     */

    setAbsoluteAssetURL() {
      if (window[configuration.globalNamespace]) {
        if (!this.data.template && window[configuration.namespace]) {
          if (
            typeof window[configuration.namespace].absoluteAssetURL !==
              "undefined" &&
            window[configuration.namespace].absoluteAssetURL !== "undefined" &&
            window[configuration.namespace].absoluteAssetURL !== ""
          ) {
            this.data.absoluteAssetURL =
              window[configuration.namespace].absoluteAssetURL;
          }
        } else {
          this.data.absoluteAssetURL = this.data.templateURL;
        }
      } else if (
        !window[configuration.globalNamespace] &&
        window[configuration.namespace]
      ) {
        if (
          typeof window[configuration.namespace].absoluteAssetURL !==
            "undefined" &&
          window[configuration.namespace].absoluteAssetURL !== "undefined" &&
          window[configuration.namespace].absoluteAssetURL !== ""
        ) {
          this.data.absoluteAssetURL =
            window[configuration.namespace].absoluteAssetURL;
        } else {
          this.data.absoluteAssetURL = this.data.absoluteAssetURL || "./";
        }
      }
    }

    /**
     * Return a JSON object of the data
     * This is meant to be implemented by the user to save a specific set of data
     * that he knows how to serialize
     */

    exportDataToJSON() {
      return {};
    }

    /**
     * Export the object as a reusable JSON
     * @return {object} the JSON representation of the object
     */

    exportToJSON() {
      const ob = {};
      ob.exportedEvents = {};
      ob.exportedFunctions = {};
      ob.exportedDrivers = {};
      ob.childs = [];

      for (const evt in this.events) {
        ob.exportedEvents[evt] = serialize$1(this.events[evt]);
      }

      for (const func in this.functions) {
        ob.exportedFunctions[func] = serialize$1(this.functions[func]);
      }

      this.childs.forEach((child) => {
        const c = child.exportToJSON();
        ob.childs.push(c);
      });

      ob.breakpoints = this.breakpoints;
      ob.dependencies = this.dependencies;
      ob.data = {
        drivers: {},
      };
      this.drivers.forEach((d) => (ob.data.drivers[d.id] = d.exportToJSON()));
      ob.data = {
        ...ob.data,
        ...this.exportDataToJSON(),
      };
      ob.type = this.type;
      if (!default_name_regex.test(this.name)) ob.name = this.name;

      return JSON.parse(JSON.stringify(ob));
    }

    /**
     * load a JSON representation of a MultimediaObject
     * @param {object} json - a json representation of a MultimediaObject
     */

    loadFromJSON(json) {
      for (const evt in json.exportedEvents) {
        this.events[evt] = unserialize$1(json.exportedEvents[evt]);
      }

      for (const func in json.exportedFunctions) {
        this.functions[func] = unserialize$1(json.exportedFunctions[func]);
      }

      if (json.childs) {
        json.childs.forEach((child) => {
          child.data = child.data || {};
          child.data.MOParent = this;
          this.childs.push(new MultimediaObject(child));
        });
      }
      // convert Booleans to Booleans
      Object.keys(this.data).forEach((key) => {
        if (this.data[key] === "false" || this.data[key] === "true")
          this.data[key] = parseBooleans$1(this.data[key]);
      });
      this.setAbsoluteAssetURL();
    }
  }

  return MultimediaObject;

}));
